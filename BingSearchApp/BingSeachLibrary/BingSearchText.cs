﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using Bing;
using System.Threading;

namespace Bibhuti.BingSeach.Library
{
    public  class BingSearchText
    {
        private string _accountKey = "/rJUrrbF2ao=";
        private BingSearchReasult _bingSearchReasult = null;
        private List<BingSearchReasult> _lstReasult = null;
        private string  _uri = "https://api.datamarket.azure.com/Bing/Search";

        public string AccountKey { get { return _accountKey; } set { _accountKey = value; } }

        private static BingSearchText cannonSingletonInstance;
        static readonly object padlock = new object();

        private BingSearchText()
        {
 
        }
       
        public static BingSearchText GetInstance()
        {
            lock (padlock)
            {
                if (cannonSingletonInstance == null)
                {
                    cannonSingletonInstance = new BingSearchText();
                }
                return cannonSingletonInstance;
            }
        }


        public List<BingSearchReasult> SearchTextBing(string query)
        {
            _lstReasult = new List<BingSearchReasult>();
            try 
	        {	   
                // Create a Bing container.          
                var bingContainer = new Bing.BingSearchContainer(new Uri(_uri));
                var accountKey = _accountKey;
                // Configure bingContainer to use your credentials.
                bingContainer.Credentials = new NetworkCredential(accountKey, accountKey);
                // Build the query.
                var searchQuery = bingContainer.Web(query, null,null, null, null, null,null,null);
                var searchResults = searchQuery.Execute();
                              
                foreach (var result in searchResults)
                {
                    _bingSearchReasult = new BingSearchReasult();
                    _bingSearchReasult.Title = result.Title;
                    _bingSearchReasult.Description = result.Description;
                    _bingSearchReasult.Url = result.Url;
                    _lstReasult.Add(_bingSearchReasult);
                }
	        }
	        catch (Exception ex)
	        {
		
		        throw ex;
	        }

            return _lstReasult;

        }
    }
}
