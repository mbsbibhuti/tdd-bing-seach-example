﻿using Bibhuti.BingSeach.Library;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace Bibhuti.BingSeach.TDDApp
{
    
    
    /// <summary>
    ///This is a test class for BingSearchTextTest and is intended
    ///to contain all BingSearchTextTest Unit Tests
    ///</summary>
    [TestClass()]
    public class BingSearchTextTest
    {


        private TestContext testContextInstance;
        private static BingSearchText _bingSearchText;

        /// <summary>
        /// 
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }



     
        [ClassInitialize()]
        public static void BingSearchTextInitialize(TestContext testContext)
        {
            _bingSearchText = BingSearchText.GetInstance();
        }


        /// <summary>
        ///A test for BingSearchText Constructor
        ///</summary>
        [TestMethod()]
        public void BingSearchTextConstructorTest()
        {
           
           
        }

        /// <summary>
        ///A test for SearchTextBing
        ///</summary>
        [TestMethod()]
        public void SearchTextBingTest()
        {
            try
            {
                //Arrange
                //BingSearchText target = BingSearchText.Instance;
                List<BingSearchReasult> actual = null;
                //Act
                string query = "Apple Iphone";         
                actual = _bingSearchText.SearchTextBing(query);

                //Assert           
                Assert.IsTrue(actual.Count >= 1);
            }
            catch (AssertFailedException ex)
            {
                Assert.Fail(ex.Message);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }

        }

        /// <summary>
        ///A test for AccountKey
        ///</summary>
        [TestMethod()]
        public void AccountKeyTest()
        {
            BingSearchText target = BingSearchText.GetInstance();
            string expected = "TestKey";  
            string actual;
            target.AccountKey = expected;
            actual = target.AccountKey;
            Assert.AreEqual(expected, actual);
            
        }
    }
}
