﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net;
using Bibhuti.BingSeach.Library;
 
namespace Bibhuti.BingSearch.WPFApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class BingSearch : Window
    {
         

        public BingSearch()
        {
            InitializeComponent();
        }

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            try
            {
              BingSearchText _bingSearchText    = BingSearchText.GetInstance();
               
              lstSearch.ItemsSource = _bingSearchText.SearchTextBing(txtSearch.Text);
            }
            catch (Exception ex)
            {

                MessageBox.Show("Bing Connection Error, Please Enter Valid App Key.");
            }
          
        } 
    }
}
